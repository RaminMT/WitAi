﻿/*
 * Downloaded from Win Nevis Community
 * Author: Ramtin Jokar
 * https://www.win-nevis.com
 * 
 */
 
 using Newtonsoft.Json;
namespace WN_Speech_Recognition
{
    //{
    //  "msg_id" : "da083635-a3a8-4e97-abff-3986a2967b49",
    //  "_text" : "سلام جیگر",
    //  "entities" : { }
    //}

    public class WitResponseObject
    {
        [JsonProperty("msg_id")]
        public string MessageId { get; set; }
        [JsonProperty("_text")]
        public string Text { get; set; }
        [JsonProperty("entities")]
        public object Entities { get; set; }
    }
}
